# D-JobRunMap, service Java

Ce module permet de lancer et suivre des tâches d'exploitation de Descartes:
- Page swagger pour l'API: http(s)://xxxxxx:8081/d-jobrunmap/api/docs.html
- Tableau de bord de l'exécution des tâches: http(s)://xxxxxx:8082/dashboard
