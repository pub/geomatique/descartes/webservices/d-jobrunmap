package fr.gouv.siig.descartes.jobrun;

import org.jobrunr.storage.StorageProvider;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Duration;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;
import static org.jobrunr.jobs.states.StateName.SCHEDULED;
import static org.jobrunr.jobs.states.StateName.SUCCEEDED;
import static org.jobrunr.utils.StringUtils.substringAfter;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = DEFINED_PORT, classes = DescartesApplication.class)
public class DescartesIntegrationTest {

    @Autowired
    TestRestTemplate restTemplate;

    @Autowired
    StorageProvider storageProvider;

    @Test
    public void enqueuelistcontextdirjobTest() {
        String responseBody = enqueueJobViaRest();
        assertThat(responseBody).startsWith("Job Enqueued");

        final UUID enqueuedJobId = UUID.fromString(substringAfter(responseBody, ": "));
        await()
          .atMost(30, TimeUnit.SECONDS)
          .until(() -> storageProvider.getJobById(enqueuedJobId).hasState(SUCCEEDED));
    }

    @Test
    public void givenEndpoint_whenJobScheduled_thenJobIsScheduled() {
        String responseBody = scheduleJobViaRest(Duration.ofHours(3));
        assertThat(responseBody).startsWith("Job Scheduled");

        final UUID scheduledJobId = UUID.fromString(substringAfter(responseBody, ": "));
        await()
          .atMost(30, TimeUnit.SECONDS)
          .until(() -> storageProvider.getJobById(scheduledJobId).hasState(SCHEDULED));
    }

    private String enqueueJobViaRest() {
        return restTemplate.getForObject("http://localhost:8081/jobs/enqueue-listcontextdirjob", String.class);
    }

    private String scheduleJobViaRest(Duration duration) {
        return restTemplate.getForObject("http://localhost:8081/jobs/schedule-listcontextdirjob?when=" + duration.toString(), String.class);
    }
}
