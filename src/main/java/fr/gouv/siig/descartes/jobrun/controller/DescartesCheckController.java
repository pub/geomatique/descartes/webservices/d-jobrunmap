package fr.gouv.siig.descartes.jobrun.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.CrossOrigin;

@Controller
public class DescartesCheckController {
	
	@CrossOrigin
    @GetMapping("/version")
    public String version() {
    		return "redirect:/manage/info";
    }
    
	@CrossOrigin
    @GetMapping("/info")
    public String info() {
    		return "redirect:/manage/info";
    }
    
	@CrossOrigin
    @GetMapping("/metrics")
    public String metrics() {
    		return "redirect:/manage/metrics";
    }
    
	@CrossOrigin
    @GetMapping("/health")
    public String health() {
    		return "redirect:/manage/health";
    }
    
	@CrossOrigin
    @GetMapping("/prometheus")
    public String prometheus() {
    		return "redirect:/manage/prometheus";
    }
}
