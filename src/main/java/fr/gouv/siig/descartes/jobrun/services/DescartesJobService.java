package fr.gouv.siig.descartes.jobrun.services;

import org.jobrunr.jobs.annotations.Job;
import org.jobrunr.jobs.context.JobRunrDashboardLogger;
import org.jobrunr.spring.annotations.Recurring;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import static java.nio.file.StandardCopyOption.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Service
public class DescartesJobService {

    Logger logger = new JobRunrDashboardLogger(LoggerFactory.getLogger(DescartesJobService.class));
    
    @Value("${app.context.dir}")
    private String contextDir;
    
    @Value("${app.context.purgeddir}")
    private String purgedDir;
  
    private String prefixContextFileName = "dsharelinkttl";
    private String[] listTtl = {"007", "030", "090", "180"};
    
    private String templatePurgedName = "-purged-";
    private long deletedTtl = 15;

    @Recurring(id = "descartes-recurring-job-purge-contextdir", cron = "5 0 * * *")
    @Job(name = "Descartes recurring job purge contextdir")
    public void recurringPurgeContextDirJob() {
    	logger.info("*** PurgeContextDirJob démarre à " + new Date());
    	logger.info("ContextDir: " + contextDir);
        try {
            Path path = Paths.get(contextDir);
            Files.createDirectories(Paths.get(purgedDir));
            for (String ttl : listTtl){
            	moveFiles(path, ttl);
            }
	    } catch (IOException e) {
	    	logger.error("IOException", e);
        } finally {
        	logger.info("*** PurgeContextDirJob finit à " + new Date());   
        }
    }
    
    @Recurring(id = "descartes-recurring-job-delete-purgedfiles", cron = "30 0 */15 * *")
    @Job(name = "Descartes recurring job delete purgedfiles")
    public void recurringDeletePurgedFilesJob() {
    	logger.info("*** DeletePurgedFilesJob démarre à " + new Date());
    	logger.info("PurgedDir: " + purgedDir);
        try {
            Path path = Paths.get(purgedDir);
            Files.createDirectories(Paths.get(purgedDir));
            deleteFiles(path);
	    } catch (IOException e) {
	    	logger.error("IOException", e);
        } finally {
        	logger.info("*** DeletePurgedFilesJob finit à " + new Date());   
        }
    }
    
    @Job(name = "Descartes job list contextdir with variable %0", retries = 2)
    public void executeListContextDirJob(String contextDir) {
        logger.info("*** ListContextDirJob démarre à " + new Date());
        logger.info("ContextDir: " + contextDir);
        try {
        	listFiles(Paths.get(contextDir));
	    } catch (IOException e) {
	    	logger.error("IOException", e);
		} finally {
			logger.info("*** ListContextDirJob finit à " + new Date());    
        }
    }

    @Job(name = "Descartes job list purgeddir with variable %0", retries = 2)
    public void executeListPurgedDirJob(String purgedDir) {
        logger.info("*** ListPurgedDirJob démarre à " + new Date());
        logger.info("PurgedDir: " + purgedDir);
        try {
        	listFiles(Paths.get(purgedDir));
	    } catch (IOException e) {
	    	logger.error("IOException", e);
		} finally {
			logger.info("*** ListPurgedDirJob finit à " + new Date());    
        }
    }
    
    @Job(name = "Descartes job list dir with variable %0", retries = 2)
    public void executeListDirJob(String dir) {
        logger.info("*** ListDirJob démarre à " + new Date());
        logger.info("Dir: " + dir);
        try {
        	listFiles(Paths.get(dir));
	    } catch (IOException e) {
	    	logger.error("IOException", e);
		} finally {
			logger.info("*** ListDirJob finit à " + new Date());    
        }
    }

    @Job(name = "Descartes job statistical contextdir with variable %0", retries = 2)
    public void executeStatContextDirJob(String contextDir) {
        logger.info("*** StatContextDirJob démarre à " + new Date());
        logger.info("ContextDir: " + contextDir);
        Path path = Paths.get(contextDir);
        try {
            for (String ttl : listTtl){
            	statFiles(path, ttl);
            }
	    } catch (IOException e) {
	    	logger.error("IOException", e);
		} finally {
			logger.info("*** StatContextDirJob finit à " + new Date());    
        }
    }
    
    public void listFiles(Path path) throws IOException {
        Stream<Path> streampath = Files.walk(path,1).filter(path2 -> !path2.equals(path));
	    List<Path> list = streampath.collect(Collectors.toList());
	    logger.info("** nombre de fichiers : " + list.size());
	    if (list.size() > 0) {
			list.forEach(l -> logger.info(l.getFileName().toString()));
		}
		streampath.close();
    }
    
    public void moveFiles(Path path, String ttl) throws IOException {
    	String prefixFileName = prefixContextFileName + ttl;
        Stream<Path> streampath = Files.walk(path,1).filter(p -> p.getFileName().toString().startsWith(prefixFileName));
        List<Path> list = streampath.collect(Collectors.toList());
        logger.info("** nombre de fichiers partagés " + ttl + " jours: " + list.size());
		if (list.size() > 0) {
			AtomicInteger countPurgedFiles = new AtomicInteger(0);
			list.forEach(l -> {
				String filename = l.getFileName().toString();
				String fileDate = filename.replace(prefixFileName, "").split("T")[0];
				LocalDate date = LocalDate.parse(fileDate, DateTimeFormatter.BASIC_ISO_DATE);
				LocalDate currentDate = LocalDate.now();
				long diff = ChronoUnit.DAYS.between(date, currentDate);
				if (diff >= Long.parseLong(ttl)) {
				    try {
						Files.move(l, Paths.get(purgedDir + "/" + currentDate.toString().replace("-", "") + templatePurgedName + filename), REPLACE_EXISTING);
				    	logger.info(filename + " purgé avec succés");
						countPurgedFiles.getAndIncrement();
					} catch (IOException e) {
						logger.error("IOException", e);
					}

				}
			});
			logger.info("** nombre de fichiers partagés " + ttl + " jours purgés: " + countPurgedFiles);
		}
		streampath.close();
    }

    public void deleteFiles(Path path) throws IOException {
        Stream<Path> streampath = Files.walk(path,1).filter(p -> p.getFileName().toString().contains(templatePurgedName));
        List<Path> list = streampath.collect(Collectors.toList());
        logger.info("** nombre de fichiers dans le répertoire : " + list.size());
		if (list.size() > 0) {
			AtomicInteger countDeletedFiles = new AtomicInteger(0);
			list.forEach(l -> {
				String filename = l.getFileName().toString();
				String purgedFileDate = filename.split(templatePurgedName)[0];
				LocalDate date = LocalDate.parse(purgedFileDate, DateTimeFormatter.BASIC_ISO_DATE);
				LocalDate currentDate = LocalDate.now();
				long diff = ChronoUnit.DAYS.between(date, currentDate);
				if (diff >= deletedTtl) {
			        File file = new File(l.toString());
			        if (file.delete()) {
			        	logger.info(filename + " supprimé avec succés");
			        	countDeletedFiles.getAndIncrement();
			        } else {
			        	logger.info("Problème lors de la suppression du fichier " + filename);
			        }
				}
			});
			logger.info("** nombre de fichiers supprimés : " + countDeletedFiles);
		}
		streampath.close();
    }
    
    public void statFiles(Path path, String ttl) throws IOException {
    	String prefixFileName = prefixContextFileName + ttl;
        Stream<Path> streampath = Files.walk(path,1).filter(p -> p.getFileName().toString().startsWith(prefixFileName));
        List<Path> list = streampath.collect(Collectors.toList());
        logger.info("** nombre de fichiers partagés " + ttl + " jours: " + list.size());
		if (list.size() > 0) {
			list.forEach(l -> {
				String filename = l.getFileName().toString();
				String fileDate = filename.replace(prefixFileName, "").split("T")[0];
				LocalDate date = LocalDate.parse(fileDate, DateTimeFormatter.BASIC_ISO_DATE);
				long diff = ChronoUnit.DAYS.between(date, LocalDate.now());
				logger.info(Long.toString(diff) + " - " + filename);
			});
		}
		streampath.close();
    }
}
