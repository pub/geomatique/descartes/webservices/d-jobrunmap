package fr.gouv.siig.descartes.jobrun;


import java.util.Base64;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class DescartesApplicationConfiguration extends WebSecurityConfigurerAdapter {

  @Value("${app.security.enabled:true}")
  private boolean securityEnabled;
	
  @Value("${app.security.user.name}")
  private String user;

  @Value("${app.security.user.password}")
  private String password;
  
  @Bean
  public PasswordEncoder passwordEncoder(){return new BCryptPasswordEncoder();}

  // Authentication : User --> Roles
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
	if (securityEnabled) { 
    auth.inMemoryAuthentication().passwordEncoder(passwordEncoder())
      .withUser(user)
        .password(new String(Base64.getDecoder().decode(password)))
        .roles("ADMIN");
	}
  }

  // Authorization : Role -> Access
  protected void configure(HttpSecurity http) throws Exception {
	if (securityEnabled) {   
	    http
	      .httpBasic()
	        .and().authorizeRequests()
	          .antMatchers("/**")
	          	.hasRole("ADMIN")
	        .and().csrf()
	        .disable()
	          .headers()
	          .frameOptions()
	        .and().disable()
	          .sessionManagement()
	          .sessionCreationPolicy(SessionCreationPolicy.STATELESS);   
	}
  }
}
