package fr.gouv.siig.descartes.jobrun;

import java.util.Collections;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.web.filter.ForwardedHeaderFilter;

@SpringBootApplication
public class DescartesApplication {

    public static void main(String[] args) {
        SpringApplication.run(DescartesApplication.class, args);
    }
    
	//actuator
	@Bean
	public FilterRegistrationBean<ForwardedHeaderFilter> forwardedHeaderFilter() {
	    final FilterRegistrationBean<ForwardedHeaderFilter> filter = new FilterRegistrationBean<>(new ForwardedHeaderFilter());
	    filter.setName("Forwarded Header filter");
	    filter.setUrlPatterns(Collections.singletonList("/manage/*"));
	    return filter;
	}

}
