package fr.gouv.siig.descartes.jobrun;

import org.springdoc.core.SpringDocConfigProperties;
import org.springdoc.core.SpringDocConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class OpenAPIConfiguration {
   
	@Bean
	SpringDocConfiguration springDocConfiguration(){
	   return new SpringDocConfiguration();
	}
	@Bean
	public SpringDocConfigProperties springDocConfigProperties() {
	   return new SpringDocConfigProperties();
	}
	
}
