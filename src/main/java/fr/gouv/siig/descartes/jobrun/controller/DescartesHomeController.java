package fr.gouv.siig.descartes.jobrun.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class DescartesHomeController {

	@CrossOrigin
	@RequestMapping(value= {"/"})
	public @ResponseBody String home() {
		return "Descartes (D-JOBRUNMAP) WebServices: OK";
	}

}