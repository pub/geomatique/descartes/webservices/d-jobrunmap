package fr.gouv.siig.descartes.jobrun.controller;

import fr.gouv.siig.descartes.jobrun.services.DescartesJobService;


import org.jobrunr.jobs.JobId;
import org.jobrunr.scheduling.JobScheduler;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.Duration;

import static java.time.Instant.now;

@RestController
public class DescartesJobController {

    private final JobScheduler jobScheduler;
    private final DescartesJobService descartesService;
    
    @Value("${app.context.dir}")
    private String contextdir;
    
    @Value("${app.context.purgeddir}")
    private String purgedDir;

    public DescartesJobController(JobScheduler jobScheduler, DescartesJobService descartesService) {
        this.jobScheduler = jobScheduler;
        this.descartesService = descartesService;
    }

    @GetMapping("/enqueue-statcontextdirjob")
    public String enqueueStatContextDirJob() {
        final JobId enqueuedJobId = jobScheduler.enqueue(() -> descartesService.executeStatContextDirJob(contextdir));
        return "Job Enqueued: " + enqueuedJobId.toString();
    }
    
    @GetMapping("/enqueue-listcontextdirjob")
    public String enqueueListContextDirJob() {
        final JobId enqueuedJobId = jobScheduler.enqueue(() -> descartesService.executeListContextDirJob(contextdir));
        return "Job Enqueued: " + enqueuedJobId.toString();
    }
    
    @GetMapping("/enqueue-listpurgeddirjob")
    public String enqueueListPurgedDirJob() {
        final JobId enqueuedJobId = jobScheduler.enqueue(() -> descartesService.executeListPurgedDirJob(purgedDir));
        return "Job Enqueued: " + enqueuedJobId.toString();
    }
    
    @GetMapping("/enqueue-listdirjob")
    public String enqueueListDirJob(@RequestParam(value = "pathdir") String pathdir) {
        final JobId enqueuedJobId = jobScheduler.enqueue(() -> descartesService.executeListDirJob(pathdir));
        return "Job Enqueued: " + enqueuedJobId.toString();
    }
    
    @GetMapping("/schedule-statcontextdirjob")
    public String scheduleStatContextDirJob(@RequestParam(value = "when") String when) {
        final JobId scheduledJobId = jobScheduler.schedule(now().plus(Duration.parse(when)), () -> descartesService.executeStatContextDirJob(contextdir));
        return "Job Scheduled: " + scheduledJobId.toString();
    }

    @GetMapping("/schedule-listcontextdirjob")
    public String scheduleListContextDirJob(@RequestParam(value = "when") String when) {
        final JobId scheduledJobId = jobScheduler.schedule(now().plus(Duration.parse(when)), () -> descartesService.executeListContextDirJob(contextdir));
        return "Job Scheduled: " + scheduledJobId.toString();
    }

    @GetMapping("/schedule-listpurgeddirjob")
    public String scheduleListPurgedDirJob(@RequestParam(value = "when") String when) {
        final JobId scheduledJobId = jobScheduler.schedule(now().plus(Duration.parse(when)), () -> descartesService.executeListPurgedDirJob(purgedDir));
        return "Job Scheduled: " + scheduledJobId.toString();
    }
    
    @GetMapping("/schedule-listdirjob")
    public String scheduleListDirJob(
            @RequestParam(value = "pathdir") String pathdir,
            @RequestParam(value = "when") String when) {
        final JobId scheduledJobId = jobScheduler.schedule(now().plus(Duration.parse(when)), () -> descartesService.executeListDirJob(pathdir));
        return "Job Scheduled: " + scheduledJobId.toString();
    }

}
