FROM eclipse-temurin:11-jdk-alpine
EXPOSE 8081 8082
ARG JAR_FILE
ADD ${JAR_FILE} app.jar
COPY ./init-data /tmp/init-data
COPY --chmod=777 ./docker-entrypoint.sh /docker-entrypoint.sh

ENTRYPOINT ["/docker-entrypoint.sh", "java","-jar","/app.jar"]
